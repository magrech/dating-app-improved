import { Component } from "@angular/core";
import { AuthService } from "../_services/auth.service";
import { AlertifyService } from "../_services/alertify.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.css"]
})
export class NavComponent {
  constructor(
    public authService: AuthService,
    private alertifyService: AlertifyService,
    private router: Router
  ) {}
  model: any = {};

  login() {
    this.authService.login(this.model).subscribe(
      () => {
        this.alertifyService.success("Logged Successfuly");
      },
      error => {
        this.alertifyService.error(error);
      }, () => {
        this.router.navigate(['/members']);
      }
    );
  }
  // loggedIn() {
  //   this.authService.loggedIn();
  // }
  loggedIn(){
    return this.authService.loggedIn()
  }
  logout() {
    localStorage.removeItem("token");
    this.alertifyService.message("Logged Out Successfuly");
    this.router.navigate(['/home']);
  }
}
